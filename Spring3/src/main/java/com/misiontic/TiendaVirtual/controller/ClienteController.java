/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.controller;

import com.misiontic.TiendaVirtual.model.Cliente;
import com.misiontic.TiendaVirtual.service.ClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/cliente")
public class ClienteController {
   
    @Autowired
    private ClienteService clienteService;
    
    @GetMapping(value = "/list")
    public List<Cliente> listarClientes() {
        return clienteService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Cliente consultarPorId(@PathVariable Integer id) {
        return clienteService.findById(id);
    }
    
    @GetMapping(value = "/list/name/{name}")
    public Cliente consultarPorId(@PathVariable String name) {
        return clienteService.findByName(name);
    }
    @PostMapping(value ="/")
    public ResponseEntity<Cliente>agregar (@RequestBody Cliente cliente){
            Cliente result = clienteService.save(cliente);
            return new ResponseEntity<>(result, HttpStatus.OK);        
    }
    @PutMapping(value = "/")
    public ResponseEntity<Cliente> editar(@RequestBody Cliente nuevo) {

        Cliente actual = clienteService.findById(nuevo.getIdCliente());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            actual.setDocumentoCliente(nuevo.getDocumentoCliente());
            actual.setNombreCliente(nuevo.getNombreCliente());
            actual.setApellidoCliente(nuevo.getApellidoCliente());
            actual.setDireccion(nuevo.getDireccion());
            actual.setCorreo(nuevo.getCorreo());
            actual.setTelefono(nuevo.getTelefono());
            clienteService.save(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Cliente> eliminar(@PathVariable Integer id) {

        Cliente result = clienteService.findById(id);

        if (result == null) {
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            clienteService.delete(id);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }
    
}
