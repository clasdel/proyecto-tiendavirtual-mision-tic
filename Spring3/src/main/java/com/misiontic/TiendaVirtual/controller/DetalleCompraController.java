/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.controller;

import com.misiontic.TiendaVirtual.model.DetalleCompra;
import com.misiontic.TiendaVirtual.service.DetalleCompraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("*")
@RequestMapping("/detalleCompra")
public class DetalleCompraController {
    
    @Autowired
    private DetalleCompraService detalleCompraService;


    @GetMapping(value = "/list")
    public List<DetalleCompra> listarDetalles() {
        return detalleCompraService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public DetalleCompra consultarPorId(@PathVariable Integer id) {
        return detalleCompraService.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<DetalleCompra> agregar(@RequestBody DetalleCompra detalleCompra) {
        DetalleCompra result = detalleCompraService.save(detalleCompra);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<DetalleCompra> editar(@RequestBody DetalleCompra nuevo) {

        DetalleCompra actual = detalleCompraService.findById(nuevo.getIdDetalleCompra());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            actual.setFacturaCompra( nuevo.getFacturaCompra());
            actual.setCantidad( nuevo.getCantidad());
            actual.setPrecio(nuevo.getPrecio());
            actual.setTotalValor( nuevo.getCantidad() * nuevo.getPrecio() );

            detalleCompraService.save(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<DetalleCompra> eliminar(@PathVariable Integer id) {

        DetalleCompra result = detalleCompraService.findById(id);

        if (result == null) {
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            detalleCompraService.delete(id);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

}


