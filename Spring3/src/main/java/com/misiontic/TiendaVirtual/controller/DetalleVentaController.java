/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.controller;

import com.misiontic.TiendaVirtual.model.DetalleVenta;
import com.misiontic.TiendaVirtual.service.DetalleVentaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/detalleVenta")
public class DetalleVentaController {
    
    @Autowired
    private DetalleVentaService detalleVentaService;

    @GetMapping(value = "/list")
    public List<DetalleVenta> listarDetalles() {
        return detalleVentaService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public DetalleVenta consultarPorId(@PathVariable Integer id) {
        return detalleVentaService.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<DetalleVenta> agregar(@RequestBody DetalleVenta detalleVenta) {
        DetalleVenta result = detalleVentaService.save(detalleVenta);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<DetalleVenta> editar(@RequestBody DetalleVenta nuevo) {

        DetalleVenta actual = detalleVentaService.findById(nuevo.getIdDetalleVenta());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            actual.setFacturaVenta( nuevo.getFacturaVenta());
            actual.setCantidad( nuevo.getCantidad());
            actual.setPrecio(nuevo.getPrecio());
            actual.setTotalValor( nuevo.getCantidad() * nuevo.getPrecio() );

            detalleVentaService.save(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<DetalleVenta> eliminar(@PathVariable Integer id) {

        DetalleVenta result = detalleVentaService.findById(id);

        if (result == null) {
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            detalleVentaService.delete(id);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }
}
