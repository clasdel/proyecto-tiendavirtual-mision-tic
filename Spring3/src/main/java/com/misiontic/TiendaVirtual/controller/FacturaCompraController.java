/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.controller;


import com.misiontic.TiendaVirtual.model.FacturaCompra;
import com.misiontic.TiendaVirtual.service.FacturaCompraService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/facturaCompra")
public class FacturaCompraController {
    
    @Autowired
    private FacturaCompraService facturaCompraService;

    @GetMapping(value = "/list")
    public List<FacturaCompra> listarFacturaCompras() {
        return facturaCompraService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public FacturaCompra consultarPorId(@PathVariable Integer id) {
        return facturaCompraService.findById(id);
    }

    @PostMapping(value ="/")
    public ResponseEntity<FacturaCompra>agregar (@RequestBody FacturaCompra facturaCompra){
            FacturaCompra result= facturaCompraService.save(facturaCompra);
            return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<FacturaCompra> editar(@RequestBody FacturaCompra nuevo) {

        FacturaCompra actual = facturaCompraService.findById(nuevo.getIdFacturaCompra());

        if (actual == null) {
            ResponseEntity response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al editar, objeto no encontrado con Id:" + nuevo.getIdFacturaCompra());
            return response;
        } else {

            TimeZone tz = TimeZone.getTimeZone("GMT-5:00");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'");
            df.setTimeZone(tz);
            String nowAsISO = df.format(new Date());

            actual.setProveedor(nuevo.getProveedor());
            actual.setFecha( nowAsISO );
            actual.setMediodePago(nuevo.getMediodePago());
            facturaCompraService.save(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<FacturaCompra> eliminar(@PathVariable Integer id) {

        FacturaCompra actual = facturaCompraService.findById(id);

        if (actual == null) {
            ResponseEntity response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al eliminar, objeto no encontrado con Id:" + actual.getIdFacturaCompra());
            return response;
        } else {
            facturaCompraService.delete(id);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

}
