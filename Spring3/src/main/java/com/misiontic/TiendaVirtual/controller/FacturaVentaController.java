/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.controller;


import com.misiontic.TiendaVirtual.model.FacturaVenta;
import com.misiontic.TiendaVirtual.service.FacturaVentaService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/facturaVenta")
public class FacturaVentaController {
    
    @Autowired
    private FacturaVentaService facturaVentaService;

    @GetMapping(value = "/list")
    public List<FacturaVenta> listarFacturaVentas() {
        return facturaVentaService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public FacturaVenta consultarPorId(@PathVariable Integer id) {
        return facturaVentaService.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<FacturaVenta> agregar(@RequestBody FacturaVenta facturaVenta) {
        FacturaVenta resultado = facturaVentaService.save(facturaVenta);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<FacturaVenta> editar(@RequestBody FacturaVenta nuevo) {

        FacturaVenta actual = facturaVentaService.findById(nuevo.getIdFacturaVenta());

        if (actual == null) {
            ResponseEntity response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al editar, objeto no encontrado con Id:" + nuevo.getIdFacturaVenta());
            return response;
        } else {

            TimeZone tz = TimeZone.getTimeZone("GMT-5:00");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'");
            df.setTimeZone(tz);
            String nowAsISO = df.format(new Date());

            actual.setCliente(nuevo.getCliente());
            actual.setMediodePago(nuevo.getMediodePago());
            actual.setFecha( nowAsISO );
            facturaVentaService.save(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<FacturaVenta> eliminar(@PathVariable Integer id) {

        FacturaVenta actual = facturaVentaService.findById(id);

        if (actual == null) {
            ResponseEntity response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al eliminar, objeto no encontrado con Id:" + actual.getIdFacturaVenta());
            return response;
        } else {
            facturaVentaService.delete(id);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }
}
