/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.controller;

import com.misiontic.TiendaVirtual.model.Proveedor;
import com.misiontic.TiendaVirtual.service.ProveedorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/proveedor")
public class ProveedorController {
    
    @Autowired
    private ProveedorService proveedorService;
    
    @GetMapping(value = "/list")
    public List<Proveedor> listarProveedores() {
        return proveedorService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Proveedor consultarPorId(@PathVariable Integer id) {
        return proveedorService.findById(id);
    }
    
    @GetMapping(value = "/list/name/{name}")
    public Proveedor consultarPorId(@PathVariable String name) {
        return proveedorService.findByName(name);
    }
    @PostMapping(value ="/")
    public ResponseEntity<Proveedor>agregar (@RequestBody Proveedor proveedor){
            Proveedor result = proveedorService.save(proveedor);
            return new ResponseEntity<>(result, HttpStatus.OK);        
    }
    @PutMapping(value = "/")
    public ResponseEntity<Proveedor> editar(@RequestBody Proveedor nuevo) {

        Proveedor actual = proveedorService.findById(nuevo.getIdProveedor());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            actual.setDocumentoProveedor(nuevo.getDocumentoProveedor());
            actual.setNombreProveedor(nuevo.getNombreProveedor());
            actual.setApellidoProveedor(nuevo.getApellidoProveedor());
            actual.setDireccion(nuevo.getDireccion());
            actual.setCorreo(nuevo.getCorreo());
            actual.setTelefono(nuevo.getTelefono());
            proveedorService.save(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Proveedor> eliminar(@PathVariable Integer id) {

        Proveedor result = proveedorService.findById(id);

        if (result == null) {
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            proveedorService.delete(id);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }
}
