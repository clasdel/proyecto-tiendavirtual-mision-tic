/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

// es una entidad llemada producto representada en la BD
@Entity
@Table(name="detalleCompra")
@Getter
@Setter
public class DetalleCompra implements Serializable {
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)//Autocrenmental
    @Column(name="idDetalleCompra")
    private Integer idDetalleCompra;
    
    @ManyToOne
    @JoinColumn(name="idFacturaCompra")
    private FacturaCompra facturaCompra;
    
    @Column(name="cantidad")
    private int cantidad;
    
    @Column(name="precio")
    private double precio;
    
    @Column(name="TotalValor")
    private double TotalValor;
}
