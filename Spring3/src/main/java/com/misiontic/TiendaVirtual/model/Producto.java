/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.model;

//clase modelo

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

// es una entidad llemada producto representada en la BD
@Entity
@Table(name="producto")
@Getter
@Setter
public class Producto implements Serializable {
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)//Autocrenmental
    @Column(name="idProducto")
    private Integer idProducto;
    
    @Column(name="nombreProducto")
    private String nombreProducto;
     
    @Column(name="unidadMedida")
    private String unidadMedida;
    
    @Column(name="precio")
    private double precio;
    
    @Column(name="stock")
    private int stock;
    
    @ManyToOne
    @JoinColumn(name="idDetalleVenta")
    private DetalleVenta detalleVenta;
    
    @ManyToOne
    @JoinColumn(name="idDetalleCompra")
    private DetalleCompra detalleCompra;
   
    
}
