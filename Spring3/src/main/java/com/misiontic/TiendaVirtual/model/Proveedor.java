/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

// es una entidad llemada producto representada en la BD
@Entity
@Table(name="proveedor")
@Getter
@Setter
public class Proveedor implements Serializable{
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)//Autocrenmental
    @Column(name="idProveedor")
    private Integer idProveedor;
    
    @Column(name="documentoProveedor")
    private double documentoProveedor;
    
    @Column(name="nombreProveedor")
    private String nombreProveedor;
     
    @Column(name="apellidoProveedor")
    private String apellidoProveedor;
    
    @Column(name="direccion")
    private String direccion;
    
    @Column(name="correo")
    private String correo;
    
    @Column(name="telefono")
    private double telefono;
    
}
