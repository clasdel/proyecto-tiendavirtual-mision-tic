/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.repository;

import com.misiontic.TiendaVirtual.model.Producto;
import org.springframework.data.repository.CrudRepository;

// extends herencia 
public interface ProductoDao extends CrudRepository <Producto, Integer> {
    
    public Producto findByNombreProducto( String name);
}
