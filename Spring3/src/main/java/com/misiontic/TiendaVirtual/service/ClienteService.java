/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service;

import com.misiontic.TiendaVirtual.model.Cliente;
import java.util.List;


public interface ClienteService {
    
    public Cliente save(Cliente cliente);
    public void delete(Integer id);
    
    public Cliente findById(Integer id);
    public List<Cliente>findAll();
    
    public Cliente findByName( String name );
}
