/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service;

import com.misiontic.TiendaVirtual.model.DetalleCompra;
import java.util.List;


public interface DetalleCompraService {
    
    public DetalleCompra save(DetalleCompra detalleCompra);
    public void delete(Integer id);
    
    public DetalleCompra  findById(Integer id);
    public List<DetalleCompra>findAll();    
    
}
