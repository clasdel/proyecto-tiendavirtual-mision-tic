/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service;

import com.misiontic.TiendaVirtual.model.DetalleVenta;
import java.util.List;



public interface DetalleVentaService {
    
    public DetalleVenta save(DetalleVenta detalleVenta);
    public void delete(Integer id);
    
    public DetalleVenta  findById(Integer id);
    public List<DetalleVenta>findAll();
}
