/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service;

import com.misiontic.TiendaVirtual.model.FacturaCompra;
import java.util.List;


public interface FacturaCompraService {
    
    public FacturaCompra save(FacturaCompra facturaCompra);
    public void delete(Integer id);
    
    public FacturaCompra findById(Integer id);
    public List<FacturaCompra>findAll();
    
}
