/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service;

import com.misiontic.TiendaVirtual.model.FacturaVenta;
import java.util.List;


public interface FacturaVentaService {
    
    public FacturaVenta save(FacturaVenta facturaVenta);
    public void delete(Integer id);
    
    public FacturaVenta  findById(Integer id);
    public List<FacturaVenta >findAll();
}
