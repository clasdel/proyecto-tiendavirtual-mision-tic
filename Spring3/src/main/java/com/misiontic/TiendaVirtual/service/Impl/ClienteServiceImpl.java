/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service.Impl;

import com.misiontic.TiendaVirtual.model.Cliente;
import com.misiontic.TiendaVirtual.repository.ClienteDao;
import com.misiontic.TiendaVirtual.service.ClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service // comunicacion con el dao y service
public class ClienteServiceImpl implements ClienteService {
    
     @Autowired
    private ClienteDao clienteDao;

    @Override
    @Transactional(readOnly =false)
    public Cliente save(Cliente cliente) {
        return clienteDao.save(cliente);
    }

    @Override
    @Transactional(readOnly =false)
    public void delete(Integer id) {
        clienteDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly =true)
    public Cliente findById(Integer id) {
        return clienteDao.findById(id).orElse(null);
    }

    @Override
    public List<Cliente> findAll() {
        return (List<Cliente>) clienteDao.findAll();
    }
     
    @Override
    public Cliente findByName(String name) {
        return clienteDao.findByNombreCliente(name);
    }
     
}
