/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service.Impl;

import com.misiontic.TiendaVirtual.model.DetalleCompra;
import com.misiontic.TiendaVirtual.repository.DetalleCompraDao;
import com.misiontic.TiendaVirtual.service.DetalleCompraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DetalleCompraServiceImpl implements DetalleCompraService {
    
    @Autowired
    private DetalleCompraDao detalleCompraDao;

    @Override
    @Transactional(readOnly =false)
    public DetalleCompra save(DetalleCompra detalleCompra) {
        return detalleCompraDao.save(detalleCompra);
    }

    @Override
    @Transactional(readOnly =false)
    public void delete(Integer id) {
        detalleCompraDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly =true)
    public DetalleCompra findById(Integer id) {
        return (DetalleCompra) detalleCompraDao.findById(id).orElse(null);
    }

    @Override
    public List<DetalleCompra> findAll() {
        return (List<DetalleCompra>)detalleCompraDao.findAll();
        
    }

   
    }

    

