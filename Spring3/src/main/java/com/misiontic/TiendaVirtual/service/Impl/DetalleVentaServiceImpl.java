/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service.Impl;

import com.misiontic.TiendaVirtual.model.DetalleVenta;
import com.misiontic.TiendaVirtual.repository.DetalleVentaDao;
import com.misiontic.TiendaVirtual.service.DetalleVentaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class DetalleVentaServiceImpl implements DetalleVentaService {
    
    @Autowired
    private DetalleVentaDao detalleVentaDao;

    @Override
    @Transactional(readOnly =false)
    public DetalleVenta save(DetalleVenta detalleVenta) {
        return detalleVentaDao.save(detalleVenta);
    }

    @Override
    @Transactional(readOnly =false)
    public void delete(Integer id) {
        detalleVentaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly =true)
    public DetalleVenta findById(Integer id) {
        return (DetalleVenta) detalleVentaDao.findById(id).orElse(null);
    }

    @Override
    public List<DetalleVenta> findAll() {
        return (List<DetalleVenta>) detalleVentaDao.findAll();
    }
}
