/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service.Impl;

import com.misiontic.TiendaVirtual.model.FacturaCompra;
import com.misiontic.TiendaVirtual.repository.FacturaCompraDao;
import com.misiontic.TiendaVirtual.service.FacturaCompraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class FacturaCompraServiceImpl implements FacturaCompraService {
    
    @Autowired
    private FacturaCompraDao facturaCompraDao;

    @Override
    @Transactional(readOnly =false)
    public FacturaCompra save(FacturaCompra facturaCompra) {
        return facturaCompraDao.save(facturaCompra);
    }

    @Override
    @Transactional(readOnly =false)
    public void delete(Integer id) {
        facturaCompraDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly =true)
    public FacturaCompra findById(Integer id) {
        return facturaCompraDao.findById(id).orElse(null);
    }

    @Override
    public List<FacturaCompra> findAll() {
        return (List<FacturaCompra>) facturaCompraDao.findAll();
    }
}
