/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service.Impl;

import com.misiontic.TiendaVirtual.model.FacturaVenta;
import com.misiontic.TiendaVirtual.repository.FacturaVentaDao;
import com.misiontic.TiendaVirtual.service.FacturaVentaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class FacturaVentaServiceImpl implements FacturaVentaService {
    
    @Autowired
    private FacturaVentaDao facturaVentaDao;

    @Override
    @Transactional(readOnly =false)
    public FacturaVenta save(FacturaVenta facturaVenta) {
        return facturaVentaDao.save(facturaVenta);
    }

    @Override
    @Transactional(readOnly =false)
    public void delete(Integer id) {
       facturaVentaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly =true)
    public FacturaVenta findById(Integer id) {
        return facturaVentaDao.findById(id).orElse(null);
    }

    @Override
    public List<FacturaVenta> findAll() {
       return (List<FacturaVenta>) facturaVentaDao.findAll();
    }
}
