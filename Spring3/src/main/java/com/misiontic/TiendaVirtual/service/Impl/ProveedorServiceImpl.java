/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service.Impl;

import com.misiontic.TiendaVirtual.model.Proveedor;
import com.misiontic.TiendaVirtual.repository.ProveedorDao;
import com.misiontic.TiendaVirtual.service.ProveedorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProveedorServiceImpl implements ProveedorService  {
    
    @Autowired
    private ProveedorDao proveedorDao;

    @Override
    @Transactional(readOnly =false)
    public Proveedor save(Proveedor proveedor) {
        return proveedorDao.save(proveedor);
    }

    @Override
    @Transactional(readOnly =false)
    public void delete(Integer id) {
        proveedorDao.deleteById(id);
        
    }

    @Override
    @Transactional(readOnly =true)
    public Proveedor findById(Integer id) {
        return proveedorDao.findById(id).orElse(null);
    }

    @Override
    public List<Proveedor> findAll() {
        return (List<Proveedor>) proveedorDao.findAll();
    }
    
    @Override
    public Proveedor findByName(String name) {
        return proveedorDao.findByNombreProveedor(name);
    }
            
}
