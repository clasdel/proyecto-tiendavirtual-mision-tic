/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service;

import com.misiontic.TiendaVirtual.model.Producto;
import java.util.List;


public interface ProductoService {
    
    public Producto save(Producto producto);
    public void delete(Integer id);
    
    public Producto findById(Integer id);
    public List<Producto>findAll();
    
    public Producto findByName( String name );
}
