/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.TiendaVirtual.service;

import com.misiontic.TiendaVirtual.model.Proveedor;
import java.util.List;


public interface ProveedorService {
    
    public Proveedor save(Proveedor proveedor);
    public void delete(Integer id);
    
    public Proveedor findById(Integer id);
    public List<Proveedor>findAll();
    
    public Proveedor findByName( String name );
}
