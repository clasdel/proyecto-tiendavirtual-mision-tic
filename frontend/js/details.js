function loadDetailsByTransactionId( id ) {

    let request = sendRequest('detalle/transaccion/'+id, 'GET', '');

    let tableHTML = document.getElementById("table-details-content");
    tableHTML.innerHTML = "";

    request.onload = function () {

        let data = request.response;

        data.forEach( row => {

            tableHTML.innerHTML += `
            
            <tr>
                        <td> ${ row.idDetalle   } </td>
                        <td> ${ row.producto.nombreProducto } </td>
                        <td class="col-right"> $ ${ row.valorDetalle } </td>
                        <td class="col-right"> $ ${ row.cantidadDetalle } </td>
                        <td class="col-right"> $ ${ row.totalDetalle } </td>
                        <td>
                            </a>
                            <a href="/admin/detalle/editar.html?id=${ row.idDetalle }" class="btn btn-sm btn-warning">
                                <i class="bi bi-pencil"></i>
                            </a> 
                            <a href="#" class="btn btn-sm btn-danger" data-bs-id="${ row.idDetalle }"
                            data-bs-toggle="modal" data-bs-target="#deleteModal" >
                                <i class="bi bi-trash"></i>
                            </a>
                        </td>
            </tr>
            
            `;

        });

    }

    request.onerror = function(){
        
        let tableHTML = document.getElementById("table-details-content");
        tableHTML.innerHTML = "";

        tableHTML.innerHTML += `

        <tr>
            <td colspan="6">
                Error al cargar los detalles
            </td>
        </tr>

        `;
    }


}