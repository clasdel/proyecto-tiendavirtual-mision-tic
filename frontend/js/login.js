function login() {

    let idUsuario = document.getElementById("idUsuario").value;
    let clave = document.getElementById("clave").value;

    let data = {
        'idUsuario': idUsuario,
        'clave': clave
    }

    let request = sendRequest('login', 'POST', data);

    request.onload = function () {

        let user = request.response;

        if (user != null) {
            window.location = '/admin/dashboard.html';
        } else {
            let myModal = new bootstrap.Modal(document.getElementById('loginErrorModal'), {});
            myModal.show();
        }

    }

    request.onerror = function () {
        alert('Se presentó un error inesperado');
    }

    return false;

}