function loadProducts() {

    let request = sendRequest('producto/list', 'GET', '');

    let tableHTML = document.getElementById("table-content");
    tableHTML.innerHTML = "";

    request.onload = function () {

        let data = request.response;

        data.forEach( row => {

            tableHTML.innerHTML += `
            
            <tr>
                        <td> ${ row.idProducto } </td>
                        <td> ${ row.nombreProducto } </td>
                        <td class="col-right"> $ ${ row.valorCompra } </td>
                        <td class="col-right"> $ ${ row.valorVenta } </td>
                        <td class="col-right"> ${ row.cantidad } </td>
                        <td>
                            <a href="/admin/producto/ver.html?id=${ row.idProducto }" class="btn btn-sm btn-success">
                                <i class="bi bi-eye"></i>
                            </a>
                            <a href="/admin/producto/editar.html?id=${ row.idProducto }" class="btn btn-sm btn-warning">
                                <i class="bi bi-pencil"></i>
                            </a> 
                            <a href="#" class="btn btn-sm btn-danger" data-bs-id="${ row.idProducto }"
                            data-bs-toggle="modal" data-bs-target="#deleteModal" >
                                <i class="bi bi-trash"></i>
                            </a>
                        </td>
            </tr>
            
            `;

        });

    }

    request.onerror = function(){
        
        let tableHTML = document.getElementById("table-content");
        tableHTML.innerHTML = "";

        tableHTML.innerHTML += `

        <tr>
            <td colspan="6">
                Error al cargar los productos
            </td>
        </tr>

        `;
    }


}

function loadProductById( id, readOnly ){

    let request = sendRequest( 'producto/list/'+id,'GET', '' );

    let inputProducto = document.getElementById("id");
    let inputNombreProducto = document.getElementById("nombreProducto");
    let inputValorCompra = document.getElementById("valorCompra");
    let inputValorVenta = document.getElementById("valorVenta");
    let inputCantidad = document.getElementById("cantidad");

    request.onload = function(){

        let data = request.response;

        if ( readOnly ){
            inputProducto.innerHTML = data.idProducto;
            inputNombreProducto.innerHTML = data.nombreProducto;
            inputValorCompra.innerHTML = data.valorCompra;
            inputValorVenta.innerHTML = data.valorVenta;
            inputCantidad.innerHTML = data.cantidad;
        }else{
            inputProducto.value = data.idProducto;
            inputNombreProducto.value = data.nombreProducto;
            inputValorCompra.value = data.valorCompra;
            inputValorVenta.value = data.valorVenta;
            inputCantidad.value = data.cantidad;
        }

    }

}

function saveProduct(){

    let nombre = document.getElementById("nombreProducto").value;
    let valorCompra = document.getElementById("valorCompra").value;
    let valorVenta = document.getElementById("valorVenta").value;
    let cantidad = document.getElementById("cantidad").value;

    let data = {
        'nombreProducto': nombre,
        'valorCompra': valorCompra,
        'valorVenta': valorVenta,
        'cantidad': cantidad
    }

    let request = sendRequest('producto/', 'POST', data);

    request.onload = function(){
        window.location = '/admin/producto/index.html';
    }

    request.onerror = function(){
        alert('Error al crear el producto');
    }


}


function editProduct(){

    let idProducto = document.getElementById("id").value;
    let nombre = document.getElementById("nombreProducto").value;
    let valorCompra = document.getElementById("valorCompra").value;
    let valorVenta = document.getElementById("valorVenta").value;
    let cantidad = document.getElementById("cantidad").value;

    let data = {
        'idProducto': idProducto,
        'nombreProducto': nombre,
        'valorCompra': valorCompra,
        'valorVenta': valorVenta,
        'cantidad': cantidad
    }

    let request = sendRequest('producto/', 'PUT', data);

    request.onload = function(){
        window.location = '/admin/producto/index.html';
    }

    request.onerror = function(){
        alert('Error al edit el producto');
    }


}


function deleteProduct( id ){

    let request = sendRequest('producto/'+id, 'DELETE', '');

    request.onload = function(){
        loadProducts();
    }


}