function loadUsers() {

    let request = sendRequest('usuario/list', 'GET', '');

    let tableHTML = document.getElementById("table-content");
    tableHTML.innerHTML = "";

    request.onload = function () {

        let data = request.response;

        data.forEach(row => {

            tableHTML.innerHTML += `
            
            <tr>
                        <td> ${ row.id } </td>
                        <td> ${ row.idUsuario } </td>
                        <!-- Sólo se toman 20 caracteres de la clave para mostrar al usuario --> 
                        <td> ${ row.clave.substring(0, 20) } ... </td>
                        <td> ${ row.nombre } </td>
                        <td> ${ row.tipo } </td>
                        <td> ${ row.estado } </td>
                        <td>
                            <a href="/admin/usuario/ver.html?id=${ row.id }" class="btn btn-sm btn-success">
                                <i class="bi bi-eye"></i>
                            </a>
                            <a href="/admin/usuario/editar.html?id=${ row.id }" class="btn btn-sm btn-warning">
                                <i class="bi bi-pencil"></i>
                            </a> 
                            <a href="#" class="btn btn-sm btn-danger" data-bs-id="${ row.id }"
                            data-bs-toggle="modal" data-bs-target="#deleteModal" >
                                <i class="bi bi-trash"></i>
                            </a>
                        </td>
            </tr>
            
            `;

        });

    }

    request.onerror = function () {

        let tableHTML = document.getElementById("table-content");
        tableHTML.innerHTML = "";

        tableHTML.innerHTML += `

        <tr>
            <td colspan="6">
                Error al cargar los usuarios
            </td>
        </tr>

        `;
    }


}

function loadUserById( idPk , readOnly) {

    let request = sendRequest('usuario/list/' + idPk, 'GET', '');

    let id = document.getElementById("id");
    let idUsuario = document.getElementById("idUsuario");
    let clave = document.getElementById("clave");
    let nombre = document.getElementById("nombre");
    let tipo = document.getElementById("tipo");
    let estado = document.getElementById("estado");

    request.onload = function () {

        let data = request.response;

        if (readOnly) {
            id.innerHTML = data.id;
            idUsuario.innerHTML = data.idUsuario;
            clave.innerHTML = data.clave;
            nombre.innerHTML = data.nombre;
            tipo.innerHTML = data.tipo;
            estado.innerHTML = data.estado;
        } else {
            id.value = data.id;
            idUsuario.value = data.idUsuario;
            clave.value = data.clave;
            nombre.value = data.nombre;
            tipo.value = data.tipo;
            estado.value = data.estado;
        }

    }

}

function saveUser() {

    let idUsuario = document.getElementById("idUsuario").value;
    let clave = document.getElementById("clave").value;
    let nombre = document.getElementById("nombre").value;
    let tipo = document.getElementById("tipo").value;
    let estado = document.getElementById("estado").value;

    let data = {
        'idUsuario': idUsuario,
        'clave': clave,
        'nombre': nombre,
        'tipo': tipo,
        'estado': estado
    }

    let request = sendRequest('usuario/', 'POST', data);

    request.onload = function () {
        window.location = '/admin/usuario/index.html';
    }

    request.onerror = function () {
        alert('Error al crear el usuario');
    }


}


function editUser() {

    let id = document.getElementById("id").value;
    let idUsuario = document.getElementById("idUsuario").value;
    let clave = document.getElementById("clave").value;
    let nombre = document.getElementById("nombre").value;
    let tipo = document.getElementById("tipo").value;
    let estado = document.getElementById("estado").value;

    let data = {
        'id': id,
        'idUsuario': idUsuario,
        'clave': clave,
        'nombre': nombre,
        'tipo': tipo,
        'estado': estado
    }

    let request = sendRequest('usuario/', 'PUT', data);

    request.onload = function () {
        window.location = '/admin/usuario/index.html';
    }

    request.onerror = function () {
        alert('Error al edit el usuario');
    }


}


function deleteUser(id) {

    let request = sendRequest('usuario/' + id, 'DELETE', '');

    request.onload = function () {
        loadUsers();
    }


}